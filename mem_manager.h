//
// Project Name: xParse
// Filename: mem_manager.h
// Creator: Yaokai Liu
// Create Date: 2023-10-31
// Copyright (c) 2023 Yaokai Liu. All rights reserved.
//

#ifndef XPARSE_MEM_MANAGER_H
#define XPARSE_MEM_MANAGER_H

#include "mem_space.h"

typedef struct mem_manager mem_manager;


extern const struct __LIUCC_MemManager__ { // NOLINT(*-reserved-identifier)
    mem_manager * (*new)(const char * name, const struct Allocator *allocator);
    const char * (*name)(mem_manager * manager);
    xSize (*space_count)(mem_manager * manager);
    xVoid (*new_space)(mem_manager * manager, const char * name, xSize element_size);
    mem_space * (*get_space)(mem_manager * manager, xuLong space_id);
    xVoid (*remove_space)(mem_manager * manager, xuLong space_id);
    xVoid * (*virt2real)(mem_manager * manager, xVoid * virt_addr);
    xVoid * (*real2virt)(mem_manager * manager, xVoid * real_addr);
} MemManager;



#endif //XPARSE_MEM_MANAGER_H
