//
// Project Name: liucc
// Filename: mem_space.c
// Creator: Yaokai Liu
// Create Date: 2023-10-16
// Copyright (c) 2023 Yaokai Liu. All rights reserved.
//
#include "mem_space.h"

#define SPACE_ALLOC_SIZE        (0x10)

typedef struct mem_space {
    const char *            space_name;
    xuInt                   space_id;
    xuInt                   element_size;
    xSize                   space_size;
    xSize                   used_size;
    const struct Allocator *allocator;
    xVoid *                 real_memory;
} mem_space;

mem_space * mem_space_new(const char * name, const struct Allocator * allocator, xSize element_size);
xVoid mem_space_del(mem_space * space);
xVoid mem_space_init(mem_space * space);
xVoid mem_space_clear(mem_space * space);
xSize mem_space_size(mem_space * space);
const char * mem_space_name(mem_space * space);
xVoid * mem_space_virt_last(mem_space * space);
xVoid mem_space_push(mem_space * space, xVoid * pElement);
xVoid mem_space_pop(mem_space * space, xVoid * pElement);
xVoid mem_space_concat(mem_space * space, const xVoid * pElement, xSize count);
xVoid mem_space_concat_space(mem_space * origin_space, mem_space * source_space);
xVoid * mem_space_real_addr(mem_space * space, xSize index);
xVoid * mem_space_virt_addr(mem_space * space, xSize index);
xVoid * mem_space_virt2real(mem_space * space, xVoid * virt_addr);
xVoid * mem_space_real2virt(mem_space * space, xVoid * real_addr);

const struct __LIUCC_MemSpace__ MemSpace = {
    .new = mem_space_new,
    .del = mem_space_del,
    .init = mem_space_init,
    .clear = mem_space_clear,
    .size = mem_space_size,
    .name = mem_space_name,
    .push = mem_space_push,
    .pop = mem_space_pop,
    .concat = mem_space_concat,
    .concat_space = mem_space_concat_space,
    .real_addr = mem_space_real_addr,
    .virt_addr = mem_space_virt_addr,
    .virt2real = mem_space_virt2real,
    .real2virt = mem_space_real2virt,
    .virt_last = mem_space_virt_last,
};


mem_space * mem_space_new(const char * name, const struct Allocator * allocator, xSize element_size) {
    thread_local static xSize STACK_ID = 0;
    mem_space * space = allocator->calloc(1, sizeof(mem_space));
    space->space_name = name;
    space->space_id = ++STACK_ID;
    space->allocator = allocator;
    space->element_size = element_size;
    return space;
}

xVoid mem_space_del(mem_space * space) {
    mem_space_clear(space);
    space->allocator->free(space);
}

xVoid mem_space_init(mem_space * space) {
    space->space_size = SPACE_ALLOC_SIZE;
    space->used_size = 0;
    space->real_memory = space->allocator->malloc(space->element_size * space->space_size);
}

xVoid mem_space_clear(mem_space * space) {
    if (space->real_memory) {
        space->allocator->free(space->real_memory);
        space->real_memory = nullptr;
        space->used_size = 0;
        space->space_size = 0;
    }
}

xSize mem_space_size(mem_space * space) {
    return space->used_size;
}

const char * mem_space_name(mem_space * space) {
    return space->space_name;
}

xVoid mem_space_push(mem_space * space, xVoid * pElement) {
    if (space->used_size + 1 >= space->space_size) {
        space->space_size += SPACE_ALLOC_SIZE;
        space->real_memory = space->allocator->realloc(space->real_memory, space->space_size * space->element_size);
    }
    xSize offset = space->used_size * space->element_size;
    space->allocator->memcpy(space->real_memory + offset, pElement, space->element_size);
    space->used_size ++;
}

xVoid mem_space_concat(mem_space * space, const xVoid * const pElement, xSize count) {
    if (space->used_size + count >= space->space_size){
        space->space_size = ((space->used_size + count) / SPACE_ALLOC_SIZE + 1) * SPACE_ALLOC_SIZE;
        space->real_memory = space->allocator->realloc(space->real_memory, space->space_size * space->element_size);
    }
    xSize offset = space->used_size * space->element_size;
    space->allocator->memcpy(space->real_memory + offset, pElement, space->element_size * count);
    space->used_size += count;
}

xVoid mem_space_concat_space(mem_space * origin_space, mem_space * source_space) {
    mem_space_concat(origin_space, source_space->real_memory, source_space->used_size);
}

xVoid mem_space_pop(mem_space * space, xVoid * pElement) {
    space->used_size --;
    xSize offset = space->element_size * space->used_size;
    if (pElement) {
        space->allocator->memcpy(pElement, space->real_memory + offset, space->element_size);
    }
    space->allocator->memset(space->real_memory + offset, 0, space->element_size);
}

xVoid * mem_space_real_addr(mem_space * space, xSize index) {
    xSize offset = index * space->element_size;
    return offset < space->used_size ? space->real_memory + offset : nullptr;
}

xVoid * mem_space_virt_addr(mem_space * space, xSize index) {
    return index >= space->used_size
            ? nullptr
            : ((xVoid *) ((index & 0x0000'0000'FFFF'FFFF)
                | (((xuLong)space->space_id) << 32)));
}

xVoid * mem_space_virt2real(mem_space * space, xVoid * virt_addr) {
    if ((((xSize)virt_addr) >> 32) == space->space_id) {
        xSize index = ((xuLong)virt_addr) & 0x0000'0000'FFFF'FFFF;
        return space->real_memory + index * space->element_size;
    } else {
        return nullptr;
    }
}

xVoid * mem_space_real2virt(mem_space * space, xVoid * real_addr) {
    xSize offset = space->used_size * space->element_size;
    if (real_addr >= space->real_memory && real_addr < space->real_memory + offset) {
        xSize index = real_addr - space->real_memory;
        if (index % space->element_size != 0) {
            return nullptr;
        }
        index /= space->element_size;
        return mem_space_virt_addr(space, index);
    } else {
        return nullptr;
    }
}

xVoid * mem_space_virt_last(mem_space * space) {
    if (!space->used_size) return nullptr;
    return mem_space_virt_addr(space, space->used_size - 1);
}