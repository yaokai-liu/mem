//
// Project Name: liucc
// Filename: mem_space.h
// Creator: Yaokai Liu
// Create Date: 2023-10-16
// Copyright (c) 2023 Yaokai Liu. All rights reserved.
//

#ifndef LIUCC_MEM_SPACE_H
#define LIUCC_MEM_SPACE_H

#include "xtypes.h"
#include "allocator.h"

typedef struct mem_space mem_space;

extern const struct __LIUCC_MemSpace__ { // NOLINT(*-reserved-identifier)
    mem_space * (*new)(const char * name, const struct Allocator * allocator, xSize element_size);
    xVoid (*del)(mem_space * space);
    xVoid (*init)(mem_space * space);
    xVoid (*clear)(mem_space * space);
    xSize (*size)(mem_space * space);
    const char * (*name)(mem_space * space);
    xVoid (*push)(mem_space * space, xVoid * pElement);
    xVoid (*pop)(mem_space * space, xVoid * pElement);
    xVoid (*concat)(mem_space * space, const xVoid * const pElement, xSize count);
    xVoid (*concat_space)(mem_space * origin_space, mem_space * source_space);
    xVoid * (*real_addr)(mem_space * space, xSize index);
    xVoid * (*virt_addr)(mem_space * space, xSize index);
    xVoid * (*virt2real)(mem_space * space, xVoid * virt_addr);
    xVoid * (*real2virt)(mem_space * space, xVoid * real_addr);
    xVoid * (*virt_last)(mem_space * space);
} MemSpace;


#define MemSpace_reinit(_space) do { MemSpace.clear(_space); MemSpace.init(_space); } while (false)

#endif //LIUCC_MEM_SPACE_H
