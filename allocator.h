//
// Project Name: liucc
// Filename: allocator.h
// Creator: Yaokai Liu
// Create Date: 2023-10-16
// Copyright (c) 2023 Yaokai Liu. All rights reserved.
//

#ifndef LIUCC_ALLOCATOR_H
#define LIUCC_ALLOCATOR_H

#include "xtypes.h"

struct Allocator {
    xVoid *(*malloc)(xSize size);
    xVoid *(*realloc)(xVoid *ptr, xSize size);
    xVoid *(*calloc)(xLong count, xSize size);
    xVoid (*free)(xVoid *mem);
    xVoid (*memcpy)(xVoid *dest, const xVoid * const src, xSize size);
    xVoid (*memset)(xVoid *mem, xuByte value, xSize size);
};


#endif //LIUCC_ALLOCATOR_H
