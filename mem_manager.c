//
// Project Name: xParse
// Filename: mem_manager.c
// Creator: Yaokai Liu
// Create Date: 2023-10-31
// Copyright (c) 2023 Yaokai Liu. All rights reserved.
//

#include "mem_manager.h"

#include <xdef.h>
#define ARRAY_ALLOC_SIZE        (0x10)
#define SPACE_ALLOC_SIZE        (0x10)


typedef struct mem_space {
    const char *            space_name;
    xuInt                   space_id;
    xuInt                   element_size;
    xSize                   space_size;
    xSize                   used_size;
    const struct Allocator *allocator;
    xVoid *                 real_memory;
} mem_space;

typedef struct mem_manager {
    const char *            manager_name;
    xuInt                   manager_id;
    xuInt                   element_size;
    xSize                   array_size;
    xSize                   used_size;
    const struct Allocator *allocator;
    mem_space *             real_array;
} mem_manager;

mem_manager * mem_manager_new(const char * name, const struct Allocator *allocator);
xSize mem_manager_space_count(mem_manager * manager);
const char * mem_manager_name(mem_manager * manager);
xVoid mem_manager_new_space(mem_manager * manager, const char * name, xSize element_size);
mem_space * mem_manager_get_space(mem_manager * manager, xuLong space_id);
xVoid mem_manager_remove_space(mem_manager * manager, xuLong space_id);
xVoid * mem_manager_virt2real(mem_manager * manager, xVoid * virt_addr);
xVoid * mem_manager_real2virt(mem_manager * manager, xVoid * real_addr);

const struct __LIUCC_MemManager__ MemManager = {
        .new = mem_manager_new,
        .name = mem_manager_name,
        .space_count = mem_manager_space_count,
        .new_space = mem_manager_new_space,
        .get_space = mem_manager_get_space,
        .remove_space = mem_manager_remove_space,
        .virt2real = mem_manager_virt2real,
        .real2virt = mem_manager_real2virt
};

mem_manager * mem_manager_new(const char * name, const struct Allocator *allocator) {
    thread_local static xSize MANAGER_ID = 0;
    mem_manager * manager = allocator->malloc(sizeof(mem_manager));
    manager->allocator = allocator;
    manager->manager_name = name;
    manager->manager_id = ++MANAGER_ID;
    manager->element_size = sizeof(mem_space);
    manager->array_size = ARRAY_ALLOC_SIZE;
    manager->used_size = 0;
    manager->real_array = allocator->malloc(manager->element_size * manager->array_size);
    return manager;
}

xSize mem_manager_space_count(mem_manager * manager) {
    return manager->used_size;
}

const char * mem_manager_name(mem_manager * manager) {
    return manager->manager_name;
}

xVoid mem_manager_new_space(mem_manager * manager, const char * name, xSize element_size) {
    #define space_init(_space)    do { \
        _space.space_name = name; \
        _space.space_id = idx; \
        _space.allocator = manager->allocator; \
        _space.element_size = element_size; \
        _space.space_size = ARRAY_ALLOC_SIZE; \
        _space.used_size = 0; \
        _space.real_memory = _space.allocator->malloc(_space.element_size * _space.space_size); \
    } while (false)

    xuInt idx = 0;
    while (idx < manager->used_size) {
        if (!manager->real_array[idx].space_name) {
            space_init(manager->real_array[idx]);
            return;
        }
        idx ++;
    }
    if (manager->used_size + 1 >= manager->array_size) {
        manager->array_size += ARRAY_ALLOC_SIZE;
        manager->real_array = manager->allocator->realloc(manager->real_array, manager->array_size * manager->element_size);
    }
    space_init(manager->real_array[manager->used_size]);
    manager->used_size ++;
    #undef space_init
}

inline mem_space * mem_manager_get_space(mem_manager * manager, xuLong space_id) {
    if (manager->used_size > space_id)
        return &manager->real_array[space_id];
    return nullptr;
}

xVoid mem_manager_remove_space(mem_manager * manager, xuLong space_id) {
    if (manager->used_size > space_id) {
        MemSpace.clear(&manager->real_array[space_id]);
        manager->allocator->memset(&manager->real_array[space_id], 0, sizeof(mem_space));
    }
}

xVoid * mem_manager_virt2real(mem_manager * manager, xVoid * virt_addr) {
    xSize space_id = (((xSize)virt_addr) >> 32);
    xSize index = ((xuLong)virt_addr) & 0x0000'0000'FFFF'FFFF;
    return (space_id < manager->used_size)
            ? MemSpace.real_addr(&manager->real_array[space_id], index)
            : nullptr;
}

xVoid * mem_manager_real2virt(mem_manager * manager, xVoid * real_addr) {
    for (xSize space_id = 0; space_id < manager->used_size; space_id ++) {
        xVoid * addr = MemSpace.real2virt(&manager->real_array[space_id], real_addr);
        if (addr) return addr;
    }
    return nullptr;
}

